"""
Configuration file
"""

from smart_env import ENV

DATABASE = ENV.DATABASE

assert isinstance(DATABASE, dict)

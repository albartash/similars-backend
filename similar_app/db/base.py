import peewee
# from playhouse.migrate import PostgresqlMigrator
from similar_app.config import DATABASE


database = peewee.PostgresqlDatabase(**DATABASE)
# migrator = PostgresqlMigrator(database)


class BaseModel(peewee.Model):
    """Base model"""

    created = peewee.TimestampField()

    class Meta:
        database = database

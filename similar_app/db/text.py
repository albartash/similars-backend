import peewee

from .base import BaseModel


class Text(BaseModel):
    """Model for texts"""
    text = peewee.TextField()

    class Meta:
        db_table = "texts"

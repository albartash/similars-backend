from flask import request
from flask_restplus import Namespace
from flask_restplus import Resource

from similar_app.db.text import Text as TextModel
from similar_app.utils import split_text_into_sentences


namespace = Namespace(
    name='Dashboard API',
    path='/api/v1/dashboard',
    description='Dashboard'
)


@namespace.route('/')
class Dashboard(Resource):
    def get(self):
        return {}


@namespace.route('/text')
class Text(Resource):
    def post(self):
        """Upload new text to the Database"""
        data = request.get_json().get('text', "")
        sentences = split_text_into_sentences(data)
        text = TextModel(text=data)
        text.save()
        return {'id': text.id, 'sentences': sentences}

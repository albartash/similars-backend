from flask import request
from flask_restplus import Namespace
from flask_restplus import Resource

from similar_app.utils import find_similar_texts


namespace = Namespace(
    name='Similar Text API',
    path='/api/v1/text',
    description='Work with similar texts'
)


@namespace.route('/similar')
class SimilarText(Resource):
    """Endpoints for managing similar texts"""

    def get(self):
        """Get list of similar texts"""

        search = request.args.get('search')
        skip_ids = request.args.get('skip_ids', "").split(',')
        if not search:
            return []

        texts = find_similar_texts(search_string=search, skip_ids=skip_ids)

        return texts

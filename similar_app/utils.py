"""
Common utils
"""
import difflib
from typing import List, Text, Tuple

import nltk
from similar_app.db.text import Text as TextModel


def split_text_into_sentences(text: Text) -> List[Text]:
    """Split text into grammar sentences"""
    return nltk.tokenize.sent_tokenize(text)


def get_similarity(a: Text, b: Text) -> float:
    return difflib.SequenceMatcher(a=a, b=b).ratio()


def find_similar_texts(search_string: Text,
                       skip_ids: List[int]
                       ) -> List[Tuple[float, Text]]:
    """Find texts with similar sentences"""

    result = []

    for text in TextModel.select(TextModel.text).where(TextModel.id.not_in(skip_ids)):
        similarity = max(
            get_similarity(t, search_string)
            for t in nltk.tokenize.sent_tokenize(text.text)
        )

        if similarity:
            result.append((similarity, text.text))

    return sorted(result, key=lambda item: item[0], reverse=True)

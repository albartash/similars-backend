from flask import Flask
from flask_cors import CORS
from flask_restplus import Api
from smart_env import ENV


def register_api(api, app):
    from similar_app.api.similarity import namespace as similarity_ns
    from similar_app.api.dashboard import namespace as dashboard_ns
    api.init_app(app)
    api.add_namespace(similarity_ns)
    api.add_namespace(dashboard_ns)


def init_nltk():
    import nltk
    nltk.download('punkt')


def create_app():
    ENV.enable_automatic_type_cast()
    init_nltk()
    app = Flask(__name__, )
    CORS(app)

    api = Api(
        version="1.0",
        doc="/doc",
        title='Similar Text Service API',
        description='Service for matching similar texts'
    )

    register_api(api, app)
    return app


app = create_app()


if __name__ == '__main__':
    app.run(
        port=5000,
        debug=ENV.DEBUG,
        host=ENV.BACKEND_HOST or '0.0.0.0'
    )
